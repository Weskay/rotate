using System.Collections;
using System.Collections.Generic;
using System.Linq;

public struct Image
{
    public byte[][] bitmap;
    public uint Width
    {
        get
        {
            return (uint)bitmap.Length;
        }
    }
    public uint Height
    {
        get
        {
            return (uint)bitmap[0].Length;
        }
    }

    public Image(uint x, uint y)
    {
        bitmap = new byte[x][];
        for (int i = 0; i < x; i++)
        {
            bitmap[i] = new byte[y];
        }

        //insert values
        var n = 0;
        for (int i = 0; i < x; i++)
        {
            for (int j = 0; j < y; j++)
            {
                bitmap[i][j] = (byte)n++;
            }
        }
    }
    
    public void Print()
    {
        for (int i = 0; i < Width; i++)
        {
            for (int k = 0; k < Height; k++)
            {
                Console.Log(bitmap[i][k]);
            }

            Console.Log("/n");
        }
    }
}

//Extension method for Images
public static class ImageModifiers
{
    /// <summary>
    /// Rotates counter clockwise
    /// </summary>
    /// <param name="image"></param>
    /// <param name="n"></param>
    public static void Rotate(ref this Image image, int n = 90)
    {
        if(n % 90 != 0)
        {
            Console.Log("please enter multiples of 90");
            return;
        }

        var rot = (Mathf.Abs(n) / 90) % 4;

        //Setting new array to handle any size by checking how many times it will rotate and creating the array  
        var tempArr = rot % 2 == 0 ?  new Image(image.Width, image.Height) : new Image(image.Height, image.Width);
        for (int times = 0; times < rot; times++)
        {
            var col = image.Width;
            for (int i = 0; i < image.Width; i++)
            {
                tempArr.bitmap[i] = image.bitmap.Select(row => row[col - 1 - i]).ToArray();
            }
        }
        image = tempArr;
    }
}

public class Rotate
{
    static public void Main(String[] args) 
    {
        var img = new Image(3, 3);
        img.Print();
        img.Rotate(90);
        img.Print();
    }
}
